package com.tetris.actions;


import com.tetris.model.Board;

public class MoveBottomAction implements Action {
    @Override
    public void run(Board board) {
        if (!board.checkMoveToBottom()) {
            board.setCurrentFigure(null);
            return;
        }
        board.removeFigure(board.getCurrentFigure());
        board.getCurrentFigure().setCoordinates(board.getCurrentFigure().getX()+1,board.getCurrentFigure().getY());
        board.putFigureOnBoard(board.getCurrentFigure());
    }
}
