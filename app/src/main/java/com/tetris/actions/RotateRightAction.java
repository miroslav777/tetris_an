package com.tetris.actions;


import com.tetris.model.Board;

public class RotateRightAction implements Action {
    @Override
    public void run(Board board) {
        if (board.checkRotateToRight()){
            board.removeFigure(board.getCurrentFigure());
            board.getCurrentFigure().rotateToRight();
            board.putFigureOnBoard(board.getCurrentFigure());
        }
    }
}
