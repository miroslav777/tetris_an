package com.tetris.actions;


import com.tetris.model.Board;

public class MoveLeftAction implements Action {
    @Override
    public void run(Board board) {
        if (board.checkMoveToLeft()) {
            board.removeFigure(board.getCurrentFigure());
            board.getCurrentFigure().setCoordinates(board.getCurrentFigure().getX(),board.getCurrentFigure().getY()-1);
            board.putFigureOnBoard(board.getCurrentFigure());
        }
    }
}
