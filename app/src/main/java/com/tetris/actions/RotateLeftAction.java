package com.tetris.actions;


import com.tetris.model.Board;

public class RotateLeftAction implements Action {
    @Override
    public void run(Board board) {
        if (board.checkRotateToLeft()){
            board.removeFigure(board.getCurrentFigure());
            board.getCurrentFigure().rotateToLeft();
            board.putFigureOnBoard(board.getCurrentFigure());
        }
    }
}
