package com.tetris.actions;


import com.tetris.model.Board;

public interface Action {
    void run(Board board);
}
