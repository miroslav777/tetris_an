package com.tetris.model.interfaces;


import com.tetris.model.Board;

public interface Rule {
    void check(Board board);
}
