package com.tetris.model.interfaces;

import com.tetris.model.Board;


public interface Listener {
    void setBoard(Board board);

    void start();

    void pause();

    void end();
}
