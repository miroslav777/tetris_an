package com.tetris.model.interfaces;


public interface CountListener {
    void setCount(int count);
}
