package com.tetris.model.figures;


public class Cube {
    private int x;
    private int y;
    private boolean isBase;

    public Cube(int x, int y, boolean isBase) {
        this.x = x;
        this.y = y;
        this.isBase = isBase;
    }

    public boolean isBase() {
        return isBase;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void increaseX(int x){
        this.x += x;
    }

    public void decreaseX(int x){
        this.x -= x;
    }

    public void increaseY(int y){
        this.y += y;
    }

    public void decreaseY(int y){
        this.y -= y;
    }



}
