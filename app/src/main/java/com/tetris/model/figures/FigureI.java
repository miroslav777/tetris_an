package com.tetris.model.figures;


public class FigureI extends Figure {

    FigureI(int x, int y, int value) {
        super(x, y, value);
    }

    @Override
    void init() {
        cubeList.clear();
        cubeList.add(baseCube);
        cubeList.add(new Cube(baseCube.getX(),baseCube.getY()-1,false));
        cubeList.add(new Cube(baseCube.getX(),baseCube.getY()+1,false));
        cubeList.add(new Cube(baseCube.getX(),baseCube.getY()+2,false));
    }
}
