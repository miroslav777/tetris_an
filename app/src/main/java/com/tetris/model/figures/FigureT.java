package com.tetris.model.figures;


public class FigureT extends Figure{

    FigureT(int x, int y, int value) {
        super(x, y, value);
    }

    @Override
    void init() {
        cubeList.clear();
        cubeList.add(baseCube);
        cubeList.add(new Cube(baseCube.getX(),baseCube.getY()-1,false));
        cubeList.add(new Cube(baseCube.getX(),baseCube.getY()+1,false));
        cubeList.add(new Cube(baseCube.getX()+1,baseCube.getY(),false));
    }
}
