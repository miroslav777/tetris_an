package com.tetris.model.figures;


import java.util.Random;

public class FigureFactory {

    private FigureFactory(){}

    public static Figure getFigure(int x, int y){
        int random = new Random().nextInt(7);

        switch (random){
            case 0: return new FigureI(x,y,1);
            case 1: return new FigureO(x,y,2);
            case 2: return new FigureL(x,y,3);
            case 3: return new FigureT(x,y,4);
            case 4: return new FigureJ(x,y,5);
            case 5: return new FigureS(x,y,6);
            case 6: return new FigureZ(x,y,7);
        }
        return null;
    }
}
