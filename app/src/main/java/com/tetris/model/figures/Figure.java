package com.tetris.model.figures;


import java.util.ArrayList;
import java.util.List;

public abstract class Figure {

    private int countCubes = 4;
    protected List<Cube> cubeList = new ArrayList<>(countCubes);
    protected Cube baseCube;
    protected int value = 1;

    Figure(int x, int y, int value){
        baseCube = new Cube(x,y,true);
        cubeList.add(baseCube);
        this.value = value;
        init();
    }

    public void draw(int differenceX, int differenceY){

        for (Cube cube: cubeList){
            if (cube.isBase()){
                continue;
            }
            cube.setX(cube.getX() + differenceX);
            cube.setY(cube.getY() + differenceY);
        }
    }

    abstract void init();

    public void setCoordinates(int x, int y){
        draw(x-baseCube.getX(),y-baseCube.getY());
        baseCube.setX(x);
        baseCube.setY(y);
    }

    public List<Cube> getCubeList() {
        return cubeList;
    }

    public int getX(){
        return baseCube.getX();
    }

    public int getY(){
        return baseCube.getY();
    }

    public boolean belong(int x, int y){
        for (Cube cube : cubeList){
            if (cube.getX() == x && cube.getY() == y){
                return true;
            }
        }
        return false;
    }

    public Cube getBaseCube(){
        return baseCube;
    }

    public void rotateToRight(){
       for (Cube cube : cubeList){
           if (cube.isBase()){
               continue;
           }

           int difx = cube.getX() - baseCube.getX();
           int dify = cube.getY() - baseCube.getY();

           if (difx == 0 && dify > 0 || difx == 0 && dify < 0){
               cube.increaseX(dify);
               cube.setY(baseCube.getY());
           }

           if (difx > 0 && dify == 0 || difx < 0 && dify == 0){
               cube.decreaseY(difx);
               cube.setX(baseCube.getX());
           }

           if (difx > 0 && dify > 0 || difx < 0 && dify < 0){
               cube.decreaseY(dify*2);
           }

           if (difx > 0 && dify < 0 || difx < 0 && dify > 0){
               cube.decreaseX(difx*2);
           }
       }
    }

    public void rotateToLeft(){
        for (Cube cube : cubeList){
            if (cube.isBase()){
                continue;
            }

            int difx = cube.getX() - baseCube.getX();
            int dify = cube.getY() - baseCube.getY();

            if (difx == 0 && dify > 0 || difx == 0 && dify < 0){
                cube.decreaseX(dify);
                cube.setY(baseCube.getY());
            }

            if (difx > 0 && dify == 0 || difx < 0 && dify == 0){
                cube.increaseY(difx);
                cube.setX(baseCube.getX());
            }

            if (difx > 0 && dify > 0 || difx < 0 && dify < 0){
                cube.decreaseX(difx*2);
            }

            if (difx > 0 && dify < 0 || difx < 0 && dify > 0){
                cube.decreaseY(dify*2);
            }
        }
    }

    public int getValue(){
        return value;
    }
}
