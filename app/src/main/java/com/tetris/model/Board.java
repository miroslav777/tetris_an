package com.tetris.model;


import com.tetris.model.figures.Cube;
import com.tetris.model.figures.Figure;
import com.tetris.model.figures.FigureFactory;

import java.util.List;

public class Board {
    private int width;
    private int height;
    private int [][] board ;
    private List<Figure> figureList;
    private Figure currentFigure;
    private BoardListener boardListener;
    private int valueOfFigure = 1;
    private boolean figureWasAddedOnBoard = false;

    private int startX = 0;
    private int startY = 3;

    public Board(int height, int width){
        this.width = width;
        this. height = height;
        board  = new int [height][width];
    }

    public int getWight() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int[][] getBoard() {
        return board;
    }

    public void addCurrentFigure(){
        currentFigure = FigureFactory.getFigure(startX,startY);
        valueOfFigure = currentFigure.getValue();
    }

    public void putCurrentFigure(){
        putFigureOnBoard(currentFigure);
    }

    public void putFigureOnBoard(Figure figure){
        for (Cube cube : figure.getCubeList()){
            board[cube.getX()][cube.getY()] = valueOfFigure;
        }
        figureWasAddedOnBoard = true;
        notifyChanges();
    }

    public boolean checkForPutCurrentFigure(){
        for (Cube cube: currentFigure.getCubeList()){
            if (board[cube.getX()][cube.getY()]!=0){
                return false;
            }
        }
        return true;
    }

    public void removeFigure(Figure figure){
        for (Cube cube : figure.getCubeList()){
            board[cube.getX()][cube.getY()] = 0;
        }
    }

    public Figure getCurrentFigure() {
        return currentFigure;
    }

    public interface BoardListener{
        void repaint();
    }

    public void setBoardListener(BoardListener boardListener){
        this.boardListener = boardListener;
    }

    public void notifyChanges(){
        if (boardListener!= null){
            boardListener.repaint();
        }
    }

    public boolean checkMoveToBottom(){
        for (Cube cube : currentFigure.getCubeList()){
            if (!currentFigure.belong(cube.getX()+1,cube.getY())){
                if (cube.getX()+1 >= height){
                    return false;
                }
                if (board[cube.getX()+1][cube.getY()] !=0){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkMoveToRight(){
        for (Cube cube : currentFigure.getCubeList()){
            if (!currentFigure.belong(cube.getX(),cube.getY()+1)){
                if (cube.getY()+1 >= width){
                    return false;
                }
                if (board[cube.getX()][cube.getY()+1] !=0){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkMoveToLeft(){

        for (Cube cube : currentFigure.getCubeList()){
            if (!currentFigure.belong(cube.getX(),cube.getY()-1)){
                if (cube.getY()-1 < 0){
                    return false;
                }
                if (board[cube.getX()][cube.getY()-1] !=0){
                    return false;
                }
            }
        }
        return true;
    }

    public void setCurrentFigure(Figure currentFigure) {
        this.currentFigure = currentFigure;
        if (currentFigure == null){
            figureWasAddedOnBoard = false;
        }
    }

    public boolean checkRotateToRight(){
        for (Cube cube : currentFigure.getCubeList()){
            if (cube.isBase()){
                continue;
            }
            
            int differenceX = cube.getX() - currentFigure.getBaseCube().getX();
            int differenceY = cube.getY() - currentFigure.getBaseCube().getY();
            int x = 0;
            int y = 0;

            if (differenceX == 0 && differenceY > 0 ){
                x = cube.getX()+differenceY;
                y = currentFigure.getBaseCube().getY();
            }

            if (differenceX > 0 && differenceY == 0){
                y = cube.getY()-differenceX;
                x = currentFigure.getBaseCube().getX();
            }

            if (differenceX == 0 && differenceY < 0){
                x = cube.getX()+differenceY;
                y = currentFigure.getBaseCube().getY();
            }

            if (differenceX < 0 && differenceY == 0){
                y = cube.getY()-differenceX;
                x = currentFigure.getBaseCube().getX();
            }

            if (differenceX > 0 && differenceY > 0){
                y =cube.getY()-differenceY*2;
            }

            if (differenceX > 0 && differenceY < 0){
                x = cube.getX()-differenceX*2;
            }

            if (differenceX < 0 && differenceY < 0){
                y = cube.getY()-differenceY*2;
            }

            if (differenceX < 0 && differenceY > 0){
                x = cube.getX()-differenceX*2;
            }

            if (currentFigure.belong(x,y)){
                continue;
            }

            if (y < 0 || y >= width || x >= height || x < 0 || board[x][y] != 0){
                return false;
            }
        }
        return true;
    }

    public boolean checkRotateToLeft(){
        for (Cube cube : currentFigure.getCubeList()){
            if (cube.isBase()){
                continue;
            }

            int differenceX = cube.getX() - currentFigure.getBaseCube().getX();
            int differenceY = cube.getY() - currentFigure.getBaseCube().getY();
            int x = 0;
            int y = 0;

            if (differenceX == 0 && differenceY > 0 ){
                x = cube.getX()-differenceY;
                y = currentFigure.getBaseCube().getY();
            }

            if (differenceX > 0 && differenceY == 0){
                y = cube.getY()+differenceX;
                x = currentFigure.getBaseCube().getX();
            }

            if (differenceX == 0 && differenceY < 0){
                x = cube.getX()-differenceY;
                y = currentFigure.getBaseCube().getY();
            }

            if (differenceX < 0 && differenceY == 0){
                y = cube.getY()+differenceX;
                x = currentFigure.getBaseCube().getX();
            }

            if (currentFigure.belong(x,y)){
                continue;
            }

            if (differenceX > 0 && differenceY > 0){
                x = cube.getX()-differenceX*2;
            }

            if (differenceX > 0 && differenceY < 0){
                y = cube.getY()-differenceY*2;
            }

            if (differenceX < 0 && differenceY < 0){
                x = cube.getX()-differenceX*2;
            }

            if (differenceX < 0 && differenceY > 0){
                y = cube.getY()-differenceY*2;
            }

            if (y < 0 || y >= width || x >= height || x < 0 || board[x][y] != 0){
                return false;
            }
        }
        return true;
    }

    public void removeLine(int line){
        for (int i = 0; i < width; i++){
            board[line][i] = 0;
        }

        for (int i = line ; i >= 0 ; i-- ){
            for (int j = 0 ; j < width ; j++){
                if (board[i][j] != 0 ){
                    int x = i+1;
                    if (board [x][j] == 0 ){
                        board[i][j] += board[x][j] ;
                        board[x][j] = board[i][j] - board[x][j];
                        board[i][j] = board[i][j] - board[x][j];
                    }
                }
            }
        }
    }

    public boolean checkLine(int line){
        for (int i = 0 ; i < width ; i++){
            if (board[line][i] == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isFigureWasAddedOnBoard() {
        return figureWasAddedOnBoard;
    }
}
