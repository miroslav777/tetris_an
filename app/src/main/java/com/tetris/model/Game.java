package com.tetris.model;


import com.tetris.actions.Action;
import com.tetris.model.interfaces.CountListener;
import com.tetris.model.interfaces.Listener;
import com.tetris.model.interfaces.Rule;
import com.tetris.model.rules.AddFigureRule;
import com.tetris.model.rules.GameOverRule;
import com.tetris.model.rules.RemoveFullLinesRule;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class Game implements Runnable {

    public static final Game game = new Game();
    private Board board;
    private LinkedBlockingQueue<Action> queueOfAction;
    private int wight = 6;
    private int height = 12;
    private boolean running;
    private FallEventGeneratorThread additionalThread;
    private Listener listener;
    private CountListener countListener;
    private int defaultTimer = 1000;
    private int count = 0;
    private List<Rule> ruleList = new ArrayList<>(1);

    private Game() {
    }

    @Override
    public void run() {

        if (running == true) {
            return;
        }

        init();
        running = true;
        listener.start();
        new Thread(additionalThread).start();
        while (running) {

            if (board.getCurrentFigure() == null) {

                for (Rule rule : ruleList) {
                    rule.check(board);
                }
            }

            Action action = null;
            try {
                action = queueOfAction.take();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (action != null) {
                action.run(board);
            }
        }
    }

    private void init() {
        if (board == null) {
            board = new Board(height, wight);
            queueOfAction = new LinkedBlockingQueue();
            additionalThread = new FallEventGeneratorThread();
            listener.setBoard(board);
            ruleList.add(new AddFigureRule());
            ruleList.add(new RemoveFullLinesRule());
            ruleList.add(new GameOverRule());
        }
    }

    public void start() {
        new Thread(this).start();
    }

    public void restart() {
        reset();
        new Thread(this).start();
    }

    public void pause() {
        running = false;
        additionalThread.stopThread();
        queueOfAction.clear();
        listener.pause();
    }

    private void reset() {
        running = false;
        queueOfAction = null;
        board = null;
        additionalThread.stopThread();
        count = 0;
        countListener.setCount(count);
        /*if (listener != null) {
            listener = null;
        }*/
    }

    public void end() {
        running = false;
        additionalThread.stopThread();
        queueOfAction.clear();
        listener.end();
    }

    public void addEvent(Action action) {
        if (running) {
            queueOfAction.add(action);
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Board getBoard() {
        return board;
    }

    public void setTimer(int time) {
        additionalThread.setInterval(time);
    }

    public void resetTimer() {
        additionalThread.setInterval(defaultTimer);
    }

    public void setCountListener(CountListener countListener) {
        this.countListener = countListener;
    }

    public void increaseCount(int sum) {
        count += sum;
        countListener.setCount(count);
    }

}
