package com.tetris.model;


import com.tetris.actions.MoveBottomAction;


public class FallEventGeneratorThread implements Runnable{

    private long interval = 1000;
    private boolean running ;

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Game.game.addEvent(new MoveBottomAction());
        }
    }

    public void stopThread(){
        running = false;
    }

    public void setInterval(int interval){
        this.interval = interval;
    }
}
