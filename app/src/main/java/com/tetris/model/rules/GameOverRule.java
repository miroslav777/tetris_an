package com.tetris.model.rules;


import com.tetris.model.Board;
import com.tetris.model.interfaces.Rule;

import static com.tetris.model.Game.game;

public class GameOverRule implements Rule{

    @Override
    public void check(Board board) {

        if (board.isFigureWasAddedOnBoard()){
            return;
        }

        if (board.checkForPutCurrentFigure()){
            board.putCurrentFigure();
        }
        else {
            game.end();
        }
    }
}
