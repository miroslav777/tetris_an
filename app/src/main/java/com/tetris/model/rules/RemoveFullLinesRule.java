package com.tetris.model.rules;


import com.tetris.model.Board;
import com.tetris.model.interfaces.Rule;

import static com.tetris.model.Game.game;

public class RemoveFullLinesRule implements Rule {

    @Override
    public void check(Board board) {

        for (int i = 0; i < board.getHeight(); i++) {
            if (board.checkLine(i)) {
                board.removeLine(i);
                game.increaseCount(1);
            }
        }
    }
}
