package com.tetris.model.rules;


import com.tetris.model.Board;
import com.tetris.model.interfaces.Rule;

public class AddFigureRule implements Rule {

    @Override
    public void check(Board board) {
        if (board.getCurrentFigure() == null){
            board.addCurrentFigure();
        }
    }
}
