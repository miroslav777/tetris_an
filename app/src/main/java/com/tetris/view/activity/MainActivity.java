package com.tetris.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tetris.R;
import com.tetris.controller.MainController;
import com.tetris.view.interfaces.MainView;

public class MainActivity extends Activity implements MainView {


    ImageButton leftButton;
    ImageButton rightButton;
    ImageButton bottomButton;
    ImageButton rotateLeftButton;
    ImageButton rotateRightButton;
    ImageButton pauseButton;
    ImageButton startButton;
    ImageButton restartButton;
    TextView textViewCount;

    private MainController mainController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        leftButton = (ImageButton) findViewById(R.id.left_button);
        rightButton = (ImageButton) findViewById(R.id.right_button);
        bottomButton = (ImageButton) findViewById(R.id.bottom_button);
        rotateLeftButton = (ImageButton)findViewById(R.id.rotate_left_butotn);
        rotateRightButton = (ImageButton) findViewById(R.id.rotate_right_button);
        pauseButton = (ImageButton) findViewById(R.id.pause_button);
        startButton = (ImageButton) findViewById(R.id.start_button);
        restartButton = (ImageButton) findViewById(R.id.restart_button);
        textViewCount = (TextView) findViewById(R.id.count);

        mainController = new MainController(this);

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });

        bottomButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        v.setSelected(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        v.setSelected(false);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        v.setSelected(true);
                        break;
                }
                mainController.eventBottomHandler(event);
                return true;
            }
        });

        rotateRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });

        rotateLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });

        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainController.eventHandler(v);
            }
        });


    }

    @Override
    public void setCount(final int count) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewCount.setText(String.valueOf(count));
            }
        });
    }

}
