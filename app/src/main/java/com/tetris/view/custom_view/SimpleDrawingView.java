package com.tetris.view.custom_view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.tetris.controller.EventController;
import com.tetris.model.Board;
import com.tetris.view.interfaces.BoardView;

public class SimpleDrawingView extends View implements BoardView , Board.BoardListener{

    private EventController eventController;
    private Board board;

    public SimpleDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        eventController = new EventController(this);
        eventController.startGame();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawMesh(canvas, board.getHeight(), board.getWight());
        drawBoard(canvas);
    }

    @Override
    public void setBoard(Board board) {
        this.board = board;
        board.setBoardListener(this);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void drawMesh(Canvas canvas, int height, int wight){
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        int heightCell = this.getHeight() / height;

        int x1 = 0;
        int y1 = 0;
        int x2 = heightCell;
        int y2 = heightCell;

        for (int i = 0; i < height; i++){
            for (int j = 0; j < wight; j++ ){
                canvas.drawRect(new Rect(x1,y1,x2,y2),paint);
                x1 += heightCell;
                x2 += heightCell;
            }
            x1 = 0;
            x2 = heightCell;
            y1 += heightCell;
            y2 += heightCell;
        }
    }

    private void drawBoard(Canvas canvas){

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);

        int heightCell = this.getHeight() / board.getHeight();

        int x1 = 0;
        int y1 = 0;
        int x2 = heightCell;
        int y2 = heightCell;

        for (int i = 0; i < board.getHeight(); i++){
            for (int j = 0; j < board.getWight(); j++ ){
                int pos = board.getBoard()[i][j];
                if (pos !=0){
                    switch (pos){
                        case 1: paint.setColor(Color.BLUE);
                            break;
                        case 2: paint.setColor(Color.YELLOW);
                            break;
                        case 3: paint.setColor(Color.GRAY);
                            break;
                        case 4: paint.setColor(Color.GREEN);
                            break;
                        case 5: paint.setColor(Color.MAGENTA);
                            break;
                        case 6: paint.setColor(Color.CYAN);
                            break;
                        case 7: paint.setColor(Color.RED);
                            break;

                    }

                    canvas.drawRect(new Rect(x1,y1,x2,y2),paint);
                }
                x1 += heightCell;
                x2 += heightCell;
            }
            x1 = 0;
            x2 = heightCell;
            y1 += heightCell;
            y2 += heightCell;
        }

    }

    @Override
    public void repaint() {
        postInvalidate();
    }

}