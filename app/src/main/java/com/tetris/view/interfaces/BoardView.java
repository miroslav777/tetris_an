package com.tetris.view.interfaces;


import com.tetris.model.Board;

public interface BoardView {
    void setBoard(Board board);
    void showMessage(String message);
}
