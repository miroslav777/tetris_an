package com.tetris.controller;


import android.os.Handler;
import android.os.Looper;

import com.tetris.model.Board;
import com.tetris.model.Game;
import com.tetris.model.interfaces.Listener;
import com.tetris.view.interfaces.BoardView;


public class EventController implements Listener {

    private BoardView boardView;
    private Game game;
    private Handler handler = new Handler(Looper.getMainLooper());


    public EventController(BoardView boardView) {
        this.boardView = boardView;
        game = Game.game;
    }

    public void startGame(){
        game.setListener(this);
        game.start();
    }

    @Override
    public void setBoard(Board board) {
        boardView.setBoard(board);
    }

    @Override
    public void start() {
       //boardView.showMessage("Start");
    }

    @Override
    public void pause() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                boardView.showMessage("Pause");
            }
        });
    }

    @Override
    public void end() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                boardView.showMessage("Game over");
            }
        });
    }

}
