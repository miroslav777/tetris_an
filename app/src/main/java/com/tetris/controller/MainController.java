package com.tetris.controller;


import android.view.MotionEvent;
import android.view.View;

import com.tetris.R;
import com.tetris.actions.MoveLeftAction;
import com.tetris.actions.MoveRightAction;
import com.tetris.actions.RotateLeftAction;
import com.tetris.actions.RotateRightAction;
import com.tetris.model.Game;
import com.tetris.model.interfaces.CountListener;
import com.tetris.view.interfaces.MainView;

public class MainController implements CountListener {

    private MainView mainView;

    public MainController(MainView mainView){
        Game.game.setCountListener(this);
        this.mainView = mainView;
    }

    public void eventHandler(View v){

        switch (v.getId()){
            case R.id.left_button :
                Game.game.addEvent(new MoveLeftAction());
                break;

            case R.id.right_button :
                Game.game.addEvent(new MoveRightAction());
                break;
            case R.id.rotate_left_butotn:
                Game.game.addEvent(new RotateLeftAction());
                break;

            case R.id.rotate_right_button:
                Game.game.addEvent(new RotateRightAction());
                break;

            case R.id.pause_button:
                Game.game.pause();
                break;
            case R.id.start_button:
                Game.game.start();
                break;

            case R.id.restart_button:
                Game.game.restart();
                break;
        }
    }

    public void eventBottomHandler(MotionEvent e){
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Game.game.setTimer(50);
                break;

            case MotionEvent.ACTION_MOVE:
                Game.game.setTimer(50);
                break;

            case MotionEvent.ACTION_UP:
                Game.game.resetTimer();
                break;
        }
    }

    @Override
    public void setCount(int count) {
        mainView.setCount(count);
    }
}
